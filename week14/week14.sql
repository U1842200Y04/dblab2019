﻿load data infile “file location\\movies.csv” into table movies fields terminated by “,” enclosed by ‘”’;

load data infile “file location\\countries.csv” into table countries fields terminated by “,” enclosed by ‘”’;

load data infile “file location\\stars.csv” into table stars fields terminated by “,”;

load data infile “file location\\movie_stars.csv” into table movie_stars fields terminated by “,”;

load data infile “file location\\directors.csv” into table directors fields terminated by “,”;

load data infile “file location\\movie_directors.csv” into table movie_directors fields terminated by “,”;

load data infile “file location\\producer_countries.csv” into table producer_countries fields terminated by “,”;

load data infile “file location\\genres.csv” into table genres fields terminated by “,”;

load data infile “file location\\languages.csv” into table languages fields terminated by “,”;


show variables like “secure_file_priv”;
